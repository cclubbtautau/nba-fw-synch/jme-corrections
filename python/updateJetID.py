import os
from analysis_tools.utils import import_root
ROOT = import_root()

import correctionlib
correctionlib.register_pyroot_binding()

class updateJetID_RDFProduced():
    def __init__(self, *args, **kwargs):
        self.year = kwargs.pop("year")
        self.runPeriod = kwargs.pop("runPeriod")
        self.NanoAODv = kwargs.pop("NanoAODv")

        # Load correctionWrapper
        if not os.getenv("_corr"):
            os.environ["_corr"] = "_corr"
            if "/libBaseModules.so" not in ROOT.gSystem.GetLibraries():
                ROOT.gInterpreter.Load("libBaseModules.so")
            base = "{}/src/Base/Modules".format(os.getenv("CMSSW_BASE"))
            if not ROOT.gInterpreter.IsLoaded("{}/interface/correctionWrapper.h".format(base)):
                ROOT.gROOT.ProcessLine(".L {}/interface/correctionWrapper.h".format(base))

        if not os.getenv("_updateJetID"):
            os.environ["_updateJetID"] = "_updateJetID"

            # Run 2: simply return the original Jet_jetId
            if self.year <= 2018:
                ROOT.gInterpreter.Declare("""
                    using Vfloat = const ROOT::RVec<float>&;
                    using Vint = const ROOT::RVec<int>&;
                    ROOT::RVec<int> get_jetId_new(
                        Vint Jet_jetId, Vfloat Jet_eta, Vfloat Jet_chHEF, Vfloat Jet_neHEF,
                        Vfloat Jet_neEmEF, Vfloat Jet_chMultiplicity, Vfloat Jet_neMultiplicity,
                        Vfloat Jet_muEF, Vfloat Jet_chEmEF)
                        {
                            return Jet_jetId;
                        }
                """)

            # Run 3: Need to differentiate between Nano Versions
            else:

                # NanoV12 -> manual recipe to re-compute the jetId
                if self.NanoAODv == "NanoAODv12":
                    ROOT.gInterpreter.Declare("""
                        using Vfloat = const ROOT::RVec<float>&;
                        using Vint = const ROOT::RVec<int>&;
                        ROOT::RVec<int> get_jetId_new(
                            Vint Jet_jetId, Vfloat Jet_eta, Vfloat Jet_chHEF, Vfloat Jet_neHEF,
                            Vfloat Jet_neEmEF, Vfloat Jet_chMultiplicity, Vfloat Jet_neMultiplicity,
                            Vfloat Jet_muEF, Vfloat Jet_chEmEF)
                            {
                                ROOT::RVec<int> Jet_jetId_new;
                                for (size_t ijet = 0; ijet < Jet_jetId.size(); ijet++)
                                {
                                    bool Jet_passJetIdTight = false;
                                    if (fabs(Jet_eta[ijet]) <= 2.7)
                                        Jet_passJetIdTight = Jet_jetId[ijet] & (1 << 1);
                                    else if (fabs(Jet_eta[ijet]) > 2.7 && fabs(Jet_eta[ijet]) <= 3.0)
                                        Jet_passJetIdTight = (Jet_jetId[ijet] & (1 << 1)) && (Jet_neHEF[ijet] < 0.99);
                                    else if (fabs(Jet_eta[ijet]) > 3.0)
                                        Jet_passJetIdTight = (Jet_jetId[ijet] & (1 << 1)) && (Jet_neEmEF[ijet] < 0.4);

                                    bool Jet_passJetIdTightLepVeto = false;
                                    if (fabs(Jet_eta[ijet]) <= 2.7)
                                        Jet_passJetIdTightLepVeto = Jet_passJetIdTight && (Jet_muEF[ijet] < 0.8) && (Jet_chEmEF[ijet] < 0.8);
                                    else
                                        Jet_passJetIdTightLepVeto = Jet_passJetIdTight;

                                    if (Jet_passJetIdTight && !Jet_passJetIdTightLepVeto)
                                        Jet_jetId_new.push_back(2);
                                    else if (Jet_passJetIdTight && Jet_passJetIdTightLepVeto)
                                        Jet_jetId_new.push_back(6);
                                    else
                                        Jet_jetId_new.push_back(0);
                                }
                                return Jet_jetId_new;
                            }
                    """)

                # NanoV13/14 -> use correctionlib to re-compute the jetId
                elif self.NanoAODv == "NanoAODv13" or self.NanoAODv == "NanoAODv14":

                    # Define correctionlib json file
                    jetId_file = "{}/{}/src/Corrections/JME/data/{}_{}_jetid.json".format(
                        os.getenv("CMT_CMSSW_BASE"), os.getenv("CMT_CMSSW_VERSION"),
                        str(self.year), self.runPeriod
                    )

                    # Define the correctionlib readers (Tight and TightLeptonVeto)
                    ROOT.gInterpreter.ProcessLine('auto corr_jetId_tight = MyCorrections("%s", "%s");' % (jetId_file, "AK4PUPPI_Tight"))
                    ROOT.gInterpreter.ProcessLine('auto corr_jetId_tightLepVeto = MyCorrections("%s", "%s");' % (jetId_file, "AK4PUPPI_TightLeptonVeto"))

                    # Compute new jetID from correctionlib
                    ROOT.gInterpreter.Declare("""
                        using Vfloat = const ROOT::RVec<float>&;
                        using Vint = const ROOT::RVec<int>&;
                        ROOT::RVec<int> get_jetId_new(
                            Vint Jet_jetId, Vfloat Jet_eta, Vfloat Jet_chHEF, Vfloat Jet_neHEF,
                            Vfloat Jet_neEmEF, Vfloat Jet_chMultiplicity, Vfloat Jet_neMultiplicity,
                            Vfloat Jet_muEF, Vfloat Jet_chEmEF)
                            {
                                ROOT::RVec<int> Jet_jetId_new;
                                for (size_t ijet = 0; ijet < Jet_jetId.size(); ijet++)
                                {
                                    float multiplicity = Jet_chMultiplicity[ijet] + Jet_neMultiplicity[ijet];
                                    float abseta = fabs(Jet_eta[ijet]);
                                    bool Jet_passJetIdTight =  corr_jetId_tight.eval(
                                        {abseta, Jet_chHEF[ijet], Jet_neHEF[ijet], Jet_neEmEF[ijet],
                                        Jet_chMultiplicity[ijet], Jet_neMultiplicity[ijet], multiplicity}
                                    );
                                    bool Jet_passJetIdTightLepVeto =  corr_jetId_tightLepVeto.eval(
                                        {abseta, Jet_chHEF[ijet], Jet_neHEF[ijet], Jet_chEmEF[ijet], Jet_neEmEF[ijet],
                                        Jet_muEF[ijet], Jet_chMultiplicity[ijet], Jet_neMultiplicity[ijet], multiplicity}
                                    );

                                    if (Jet_passJetIdTight && !Jet_passJetIdTightLepVeto)
                                        Jet_jetId_new.push_back(2);
                                    else if (Jet_passJetIdTight && Jet_passJetIdTightLepVeto)
                                        Jet_jetId_new.push_back(6);
                                    else
                                        Jet_jetId_new.push_back(0);
                                }
                                return Jet_jetId_new;
                            }
                    """)

                # Other NanoAOD versions not considered
                else:
                    raise ValueError("Required NanoAOD version %s not supported!" % self.NanoAODv)

    def run(self, df):

        # Store original Jet_jetId as `Jet_jetId_old`
        df = df.Define("Jet_jetId_old", "Jet_jetId")

        # Redefine Jet_jetId to the correct (updated) value
        df = df.Redefine("Jet_jetId", "get_jetId_new("
                         "Jet_jetId, Jet_eta, Jet_chHEF, Jet_neHEF, Jet_neEmEF,"
                         "Jet_chMultiplicity, Jet_neMultiplicity, Jet_muEF, Jet_chEmEF)")

        return df, ["Jet_jetId", "Jet_jetId_old"]


def updateJetID_RDF(**kwargs):
    """
    Module to re-compute the jetId variable which is bugged in NanoV12/13/14
    https://cms-talk.web.cern.ch/t/bug-in-the-jetid-flag-in-nanov12-and-nanov13/108135
    Different implementation is needed based on the NanoV12 and NanoV13/14:
    - NanoV12: manual recipe
    - NanoV13/14: correctionlib
    The `Jet_jetId` variable is Re-Defined to the correct value, and the
    original value is stored in `Jet_jetId_old`.

    :param NanoAODv: version of the input nanoAOD files
    :type NanoAODv: str

    YAML sintaxis:

    .. code-block:: yaml

        codename:
            name: updateJetID_RDF
            path: Corrections.JME.updateJetID
            parameters:
                year: self.config.year
                runPeriod: self.config.runPeriod
                NanoAODv: self.dataset.tags[0]
    """
    return lambda: updateJetID_RDFProduced(**kwargs)
