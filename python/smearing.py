import os
import tarfile
import tempfile
import envyaml

from analysis_tools.utils import import_root
from Base.Modules.baseModules import JetLepMetSyst

ROOT = import_root()

corrCfg = envyaml.EnvYAML('%s/src/Corrections/JME/python/smearingFiles.yaml' %
                                    os.environ['CMSSW_BASE'])

class jetSmearerRDFProducer(JetLepMetSyst):
    def __init__(self, *args, **kwargs):
        super(jetSmearerRDFProducer, self).__init__(*args, **kwargs)
        self.isMC = kwargs.pop("isMC")
        self.year = kwargs.pop("year") ; year = str(self.year)
        self.type = kwargs.pop("type", "")
        self.after_jec = kwargs.pop("after_jec", False)
        self.synchedRNG = kwargs.pop("synchedRNG", False)
        self.skip_unused_systs = kwargs.pop("skipUnusedSysts", False)

        prefix = "" ; isUL = False
        try:
            isUL = kwargs.pop("isUL")
            prefix += "" if not isUL else "UL"
        except KeyError:
            pass
        try:
            prefix += kwargs.pop("runPeriod")
        except KeyError:
            pass

        self.corrKey = prefix + year

        if self.isMC:
            jerArchive = tarfile.open(corrCfg[self.corrKey]["fileName"], "r:gz")
            jerInputFilePath = tempfile.mkdtemp()
            jerArchive.extractall(jerInputFilePath)

            corrName = corrCfg[self.corrKey]["corrName"]
            unctName = corrCfg[self.corrKey]["unctName"]
            if self.type == "fatjerc":
                corrName.replace("AK4", "AK8")
                unctName.replace("AK4", "AK8")

            if not os.getenv("_jetSmearer"):
                os.environ["_jetSmearer"] = "_jetSmearer"
                if "/libCorrectionsJME.so" not in ROOT.gSystem.GetLibraries():
                    ROOT.gSystem.Load("libCorrectionsJME.so")
                base = "{}/src/Corrections/JME".format(os.getenv("CMSSW_BASE"))
                ROOT.gROOT.ProcessLine(".L {}/interface/jetSmearer.h".format(base))

            if not os.getenv(f"_jetSmearer{self.type}"):
                os.environ[f"_jetSmearer{self.type}"] = f"_jetSmearer{self.type}"

                ROOT.gInterpreter.Declare(
                    'auto jet_smearer_%s = jetSmearer("%s", "%s", "%s");' % (
                        self.type, jerInputFilePath, corrName, unctName) )

    def run(self, df):
        if not self.isMC:
            return df, []
        all_branches = df.GetColumnNames()

        # Run-2 uses fixedGridRhoFastjetAll and Run-3 Rho_fixedGridRhoFastjetAll
        rho = "fixedGridRhoFastjetAll"
        if rho not in all_branches:
            rho = "Rho_fixedGridRhoFastjetAll"

        branches = ["jet_smear_seed", "jet_smear_factor", "jet_smear_factor_down",
                    "jet_smear_factor_up", "jet_pt_resolution"]

        pt = "Jet_pt"
        mass = "Jet_mass"
        eta = "Jet_eta"
        phi = "Jet_phi"
        jertype = "jer"
        jetsize = ""
        fatPrefix = ""
        if self.type == "fatjerc":
            pt   = "Fat" + pt
            mass = "Fat" + mass
            eta  = "Fat" + eta
            phi  = "Fat" + phi
            jertype = "fatjer"
            jetsize = "AK8"
            fatPrefix  = "fat"

        if self.after_jec:
            pt += "_corr"
            mass += "_corr"

        if self.synchedRNG:
            df = df.Define(f"__{jertype}_results", f"jet_smearer_{self.type}.get_smear_vals("
                f"event_seed, {pt}, {eta}, {phi}, {mass}, "
                f"GenJet{jetsize}_pt, GenJet{jetsize}_eta, GenJet{jetsize}_phi, "
                f"GenJet{jetsize}_mass, {rho})")

        else:
            df = df.Define(f"__{jertype}_results", f"jet_smearer_{self.type}.get_smear_vals("
                f"run, luminosityBlock, event, {pt}, {eta}, {phi}, {mass}, "
                f"GenJet{jetsize}_pt, GenJet{jetsize}_eta, GenJet{jetsize}_phi, "
                f"GenJet{jetsize}_mass, {rho})")

        branches2store = []
        for ib, branch in enumerate(branches):
            # skip all the cases for which the systematic variation would not actually be used
            if self.skip_unused_systs:
                if not "smeared" in self.jet_syst:
                    if "_up" in branch or "_down" in branch:
                        continue
                else:
                    if "_up" in branch and not "_up" in self.jet_syst:
                        continue
                    if "_down" in branch and not "_down" in self.jet_syst:
                        continue

            df = df.Define(fatPrefix+branch, f"__{jertype}_results.{branch}")
            branches2store.append(fatPrefix+branch)

        return df, branches2store


def jetSmearerRDF(**kwargs):
    """
    Module to compute jet smearing factors (nom, up, down) and jet_pt_resolution
    Note: UL2016_preVFP is not implemented

    YAML sintaxis:

    .. code-block:: yaml

        codename:
            name: jetSmearerRDF
            path: Corrections.JME.smearing
            parameters:
                year: self.config.year
                isMC: self.dataset.process.isMC
                jerTag: self.config.year
                isUL: self.dataset.has_tag('ul')
                ispreVFP: self.config.get_aux("isPreVFP", False)
    """

    return lambda: jetSmearerRDFProducer(**kwargs)


class jetVarRDFProducer(JetLepMetSyst):
    def __init__(self, *args, **kwargs):
        super(jetVarRDFProducer, self).__init__(*args, **kwargs)
        self.isMC = kwargs.pop("isMC")
        self.type = kwargs.pop("type", "")
        self.after_jec = kwargs.pop("after_jec", False)
        self.skip_unused_systs = kwargs.pop("skipUnusedSysts", False)

    def run(self, df):
        if not self.isMC:
            return df, []

        pt = "Jet_pt"
        mass = "Jet_mass"
        massSD = "FatJet_msoftdrop"
        if self.after_jec:
            pt += "_corr"
            mass += "_corr"
            massSD += "_corr"

        Fat = ""
        fat = ""
        if self.type == "fatjerc":
            Fat = "Fat"
            fat = "fat"
            pt   = "Fat" + pt
            mass = "Fat" + mass

        branches2store = []
        for branch_name, factor_name in [("_nom", ""), ("_smeared_up", "_up"), ("_smeared_down", "_down")]:
            # skip all the cases for which the systematic variation would not actually be used
            if self.skip_unused_systs:
                if not "smeared" in self.jet_syst:
                    if "_up" in branch_name or "_down" in branch_name:
                        continue
                else:
                    if "_up" in branch_name and not "_up" in self.jet_syst:
                        continue
                    if "_down" in branch_name and not "_down" in self.jet_syst:
                        continue

            df = df.Define(f"{Fat}Jet_pt{branch_name}", f"{pt} * {fat}jet_smear_factor{factor_name}")
            df = df.Define(f"{Fat}Jet_mass{branch_name}", f"{mass} * {fat}jet_smear_factor{factor_name}")
            branches2store.append(f"{Fat}Jet_pt{branch_name}")
            branches2store.append(f"{Fat}Jet_mass{branch_name}")

            if self.type == "fatjerc":
                df = df.Define(f"FatJet_msoftdrop{branch_name}", f"{massSD} * fatjet_smear_factor{factor_name}")
                branches2store.append(f"FatJet_msoftdrop{branch_name}")

        return df, branches2store


def jetVarRDF(**kwargs):
    """
    Module to compute jet pt and mass after applying smearing factors

    YAML sintaxis:

    .. code-block:: yaml

        codename:
            name: jetVarRDF
            path: Base.Modules.smearing
            parameters:
                isMC: self.dataset.process.isMC

    """
    return lambda: jetVarRDFProducer(**kwargs)


class metSmearerRDFProducer(JetLepMetSyst):
    def __init__(self, *args, **kwargs):
        super(metSmearerRDFProducer, self).__init__(*args, **kwargs)
        self.isMC = kwargs.pop("isMC")
        self.after_jec = kwargs.pop("after_jec", False)

        if self.isMC:
            if not os.getenv("_metSmearer"):
                os.environ["_metSmearer"] = "_metSmearer"
                if "/libCorrectionsJME.so" not in ROOT.gSystem.GetLibraries():
                    ROOT.gSystem.Load("libCorrectionsJME.so")
                base = "{}/{}/src/Corrections/JME".format(
                    os.getenv("CMT_CMSSW_BASE"), os.getenv("CMT_CMSSW_VERSION"))
                if not os.getenv("_METShift"):
                    os.environ["_METShift"] = "METShift"
                    ROOT.gROOT.ProcessLine(".L {}/interface/metShift.h".format(base))
                ROOT.gInterpreter.Declare('auto met_shifter = metShift();')

    def run(self, df):
        if not self.isMC:
            return df, []

        pt = "Jet_pt"
        mass = "Jet_mass"
        if self.after_jec:
            pt += "_corr"
            mass += "_corr"

        df = df.Define("smeared_met", "met_shifter.get_shifted_met("
            f"Jet_eta, Jet_phi, {pt}, {mass}, Jet_pt_nom, Jet_mass_nom, MET_pt, MET_phi)")
        df = df.Define("smeared_met_up", "met_shifter.get_shifted_met("
            f"Jet_eta, Jet_phi, {pt}, {mass}, Jet_pt_smeared_up, Jet_mass_smeared_up, MET_pt, MET_phi)")
        df = df.Define("smeared_met_down", "met_shifter.get_shifted_met("
            f"Jet_eta, Jet_phi, {pt}, {mass}, Jet_pt_smeared_down, Jet_mass_smeared_down, MET_pt, MET_phi)")

        branches = ["MET_smeared_pt", "MET_smeared_phi"]
        branches_up = ["MET_smeared_pt_up", "MET_smeared_phi_up"]
        branches_down = ["MET_smeared_pt_down", "MET_smeared_phi_down"]

        for ib, branch in enumerate(branches):
            df = df.Define(branch, "smeared_met[%s]" % ib)
        for ib, branch in enumerate(branches_up):
            df = df.Define(branch, "smeared_met_up[%s]" % ib)
        for ib, branch in enumerate(branches_down):
            df = df.Define(branch, "smeared_met_down[%s]" % ib)
        return df, branches + branches_up + branches_down


def metSmearerRDF(**kwargs):
    """
    Module to compute MET pt and phi after applying smearing to all jets
    and up and down variations

    YAML sintaxis:

    .. code-block:: yaml

        codename:
            name: metSmearerRDF
            path: Base.Modules.smearing
            parameters:
                isMC: self.dataset.process.isMC

    """
    return lambda: metSmearerRDFProducer(**kwargs)


class metJetTauSmearerRDFProducer(metSmearerRDFProducer):
    def run(self, df):
        if not self.isMC:
            return df, []

        pt = "Jet_pt"
        mass = "Jet_mass"
        if self.after_jec:
            pt += "_corr"
            mass += "_corr"

        tauptcorr = "Tau_pt" + self.tau_syst
        taumcorr  = "Tau_mass" + self.tau_syst

        df = df.Define("smeared_met", "met_shifter.get_shifted_met("
                       f"Jet_eta, Jet_phi, {pt}, {mass}, Jet_pt_nom, Jet_mass_nom, "
                       "MET_pt, MET_phi)")
        df = df.Define("tes_smeared_met", "met_shifter.get_shifted_met("
                       f"Tau_eta, Tau_phi, Tau_pt, Tau_mass, {tauptcorr}, {taumcorr}, "
                       "smeared_met[0], smeared_met[1])")
        df = df.Define("MET_smeared_pt", "tes_smeared_met[0]")
        df = df.Define("MET_smeared_phi", "tes_smeared_met[1]")
        df = df.Define("smeared_met_up", "met_shifter.get_shifted_met("
                       "Jet_eta, Jet_phi, Jet_pt_nom, Jet_mass_nom, Jet_pt_smeared_up, "
                       "Jet_mass_smeared_up, MET_smeared_pt, MET_smeared_phi)")
        df = df.Define("smeared_met_down", "met_shifter.get_shifted_met("
                       "Jet_eta, Jet_phi, Jet_pt_nom, Jet_mass_nom, Jet_pt_smeared_down, "
                       "Jet_mass_smeared_down, MET_smeared_pt, MET_smeared_phi)")
        df = df.Define("smeared_met_corr_up", "met_shifter.get_shifted_met("
                       f"Tau_eta, Tau_phi, {tauptcorr}, {taumcorr}, {tauptcorr}_up, "
                       f"{taumcorr}_up, MET_smeared_pt, MET_smeared_phi)")
        df = df.Define("smeared_met_corr_down", "met_shifter.get_shifted_met("
                       f"Tau_eta, Tau_phi, {tauptcorr}, {taumcorr}, {tauptcorr}_down, "
                       f"{taumcorr}_down, MET_smeared_pt, MET_smeared_phi)")

        branches = ["MET_smeared_pt", "MET_smeared_phi"]
        branches_up = ["MET_smeared_pt_up", "MET_smeared_phi_up"]
        branches_down = ["MET_smeared_pt_down", "MET_smeared_phi_down"]
        branches_tes_up = ["MET_smeared_pt_corr_up", "MET_smeared_phi_corr_up"]
        branches_tes_down = ["MET_smeared_pt_corr_down", "MET_smeared_phi_corr_down"]

        df = df.Define(branches_up[0], "smeared_met_up[0]")
        df = df.Define(branches_up[1], "smeared_met_up[1]")
        df = df.Define(branches_down[0], "smeared_met_down[0]")
        df = df.Define(branches_down[1], "smeared_met_down[1]")
        df = df.Define(branches_tes_up[0], "smeared_met_corr_up[0]")
        df = df.Define(branches_tes_up[1], "smeared_met_corr_up[1]")
        df = df.Define(branches_tes_down[0], "smeared_met_corr_down[0]")
        df = df.Define(branches_tes_down[1], "smeared_met_corr_down[1]")

        return df, branches + branches_up + branches_down + branches_tes_up + branches_tes_down


def metJetTauSmearerRDF(**kwargs):
    """
    Module to compute MET pt and phi after applying smearing to all jets and tau energy scale
    and up and down variations

    YAML sintaxis:

    .. code-block:: yaml

        codename:
            name: metJetTauSmearerRDF
            path: Base.Modules.smearing
            parameters:
                isMC: self.dataset.process.isMC

    """
    return lambda: metJetTauSmearerRDFProducer(**kwargs)

