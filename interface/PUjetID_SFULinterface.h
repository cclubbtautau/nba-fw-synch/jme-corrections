#ifndef PUjetID_SFInterface_h
#define PUjetID_SFInterface_h

// -------------------------------------------------------------------------------------------------------------- //
//                                                                                                                //
//   class PUjetID_SFInterface                                                                                    //
//                                                                                                                //
//   Class to compute Htt trigger scale factors.                                                                  //
//                                                                                                                //
//   Author: Jaime León Holgado                                                                                   //
//   Date  : Feb 2022                                                                                             //
//                                                                                                                //
// -------------------------------------------------------------------------------------------------------------- //

// Standard libraries
#include <vector>
#include <iostream>
#include <string>
#include <fstream>
#include <sstream>


// ROOT libraries
#include <TLorentzVector.h>
#include <ROOT/RVec.hxx>
#include <Math/VectorUtil.h>
#include <TH2.h>
#include <TFile.h>
#include "Base/Modules/interface/correctionWrapper.h"

typedef ROOT::VecOps::RVec<float> fRVec;
typedef ROOT::VecOps::RVec<bool> bRVec;
typedef ROOT::VecOps::RVec<int> iRVec;


// PUjetID_SFInterface class
class PUjetID_SFULinterface {

  public:
    PUjetID_SFULinterface (int year, std::string filename);    
    ~PUjetID_SFULinterface ();
    std::vector <double> get_pu_weights(
        fRVec Jet_pt, fRVec Jet_eta, fRVec Jet_phi, fRVec Jet_mass, iRVec Jet_jetId, iRVec Jet_puId,
        fRVec GenJet_pt, fRVec GenJet_eta, fRVec GenJet_phi, fRVec GenJet_mass,
        fRVec lep_pt, fRVec lep_eta, fRVec lep_phi, fRVec lep_mass);

  private:
    int year_;
    MyCorrections corr; 
    

};

#endif // PUjetID_SFinterface
