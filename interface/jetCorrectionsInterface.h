#ifndef jetCorrectionsInterface_h
#define jetCorrectionsInterface_h

// -------------------------------------------------------------------------------------------------------------- //
//                                                                                                                //
//   class jetCorrectionsInterface                                                                                //
//                                                                                                                //
//   Class to compute JEC values from correctionlib json.                                                         //
//                                                                                                                //
//   Author: Jona Motta (jona.motta@cern.ch)                                                                      //
//   Date  : April 2024                                                                                           //
//                                                                                                                //
// -------------------------------------------------------------------------------------------------------------- //

// Standard libraries

//CMSSW libraries
#include "Base/Modules/interface/correctionWrapper.h"
#include <TRandom3.h>
#include <TLorentzVector.h>
#include <TMath.h>
#include <ROOT/RVec.hxx>

// // libraries of deterministic seed production and RNG
#include "Base/Modules/interface/DeterministicSeedInterface.h"
#include "Base/Modules/interface/NumpySFC64GeneratorInterface.h"

typedef ROOT::VecOps::RVec<float> fRVec;
typedef ROOT::VecOps::RVec<uint64_t> ui64RVec;

// structure identical to jetSmearerOutput in jetSmearer.h
struct jerOutput {
  ui64RVec jet_smear_seed;
  fRVec    jet_smear_factor;
  fRVec    jet_smear_factor_down;
  fRVec    jet_smear_factor_up;
  fRVec    jet_pt_resolution;
};

class jetCorrections {
  public:
    // Constructors used for the JES application
    jetCorrections(std::string filename, std::string L1, std::string L2, std::string L3, std::string L2L3);
    jetCorrections(std::string filename, std::string L1, std::string L2, std::string L3);
    jetCorrections(std::string filename, std::string LSYST);
    // Constructors used for the JER application
    jetCorrections(std::string filename, std::string ptResolustionCorr, std::string smearingSfCorr);
    
    ~jetCorrections();
    
    // Functions used for the JES application
    fRVec get_jec_sf(fRVec A, fRVec eta, fRVec pt, Float_t rho, fRVec rawF);
    fRVec get_jec_sf(fRVec A, fRVec eta, fRVec phi, fRVec pt, Float_t rho, fRVec rawF);
    fRVec get_jec_sf(fRVec eta, fRVec pt, fRVec rawF, fRVec centralF, std::string drctn);
    // Functions used for the JER application
    jerOutput get_jer_outputs(int run, int luminosityBlock, int event,
                              fRVec Jet_pt, fRVec Jet_eta, fRVec Jet_phi, fRVec Jet_mass,
                              fRVec GenJet_pt, fRVec GenJet_eta, fRVec GenJet_phi, fRVec GenJet_mass,
                              float rho);
    jerOutput get_jer_outputs(uint64_t event_seed,
                              fRVec Jet_pt, fRVec Jet_eta, fRVec Jet_phi, fRVec Jet_mass,
                              fRVec GenJet_pt, fRVec GenJet_eta, fRVec GenJet_phi, fRVec GenJet_mass,
                              float rho);

  private:
    // Private variables used for the JES application
    std::string datatype_;
    MyCorrections jecL1 = MyCorrections();
    MyCorrections jecL2 = MyCorrections();
    MyCorrections jecL3 = MyCorrections();
    MyCorrections jecL2L3 = MyCorrections();
    
    // Private variables used for the JER application
    MyCorrections jerPtRes = MyCorrections();
    MyCorrections jerSmearSf = MyCorrections();
    TRandom3 rnd = TRandom3(12345);
    NumpyGenerator RNGnumpy = NumpyGenerator("SFC64");
    DeterministicSeedInterface RNGseed = DeterministicSeedInterface();
    std::vector<std::string> variations = {"nom", "down", "up"};

};

#endif // jetCorrectionsInterface_h
