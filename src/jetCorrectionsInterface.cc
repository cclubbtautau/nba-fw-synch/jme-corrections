#include "Corrections/JME/interface/jetCorrectionsInterface.h"

jetCorrections::jetCorrections(std::string filename, std::string L1, std::string L2, std::string L3, std::string L2L3) : 
  jecL1(filename, L1),
  jecL2(filename, L2),
  jecL3(filename, L3),
  jecL2L3(filename, L2L3)
{
  datatype_ = "data";
}

jetCorrections::jetCorrections(std::string filename, std::string L1, std::string L2, std::string L3) : 
  jecL1(filename, L1),
  jecL2(filename, L2),
  jecL3(filename, L3)
{
  datatype_ = "mc";
}

jetCorrections::jetCorrections(std::string filename, std::string LSYST) : 
  jecL1(filename, LSYST)
{
  datatype_ = "mc";
}

jetCorrections::jetCorrections(std::string filename, std::string ptResolustionCorr, std::string smearingSfCorr) : 
  jerPtRes(filename, ptResolustionCorr),
  jerSmearSf(filename, smearingSfCorr)
{}

// Destructor
jetCorrections::~jetCorrections() {}

fRVec jetCorrections::get_jec_sf(fRVec A, fRVec eta, fRVec pt, Float_t rho, fRVec rawF)
{
  fRVec jec_sf;
  for (size_t i = 0; i < pt.size(); i++) {
      double jetEta = eta[i];
      double ptRaw = pt[i] * (1 - rawF[i]);

      double ptL1 = ptRaw * jecL1.eval({A[i], jetEta, ptRaw, rho});
      double ptL2 = ptL1  * jecL2.eval({jetEta, ptL1});
      double ptL3 = ptL2  * jecL3.eval({jetEta, ptL2});

      if (datatype_ == "data") {
        double ptL2L3 = ptL3  * jecL2L3.eval({jetEta, ptL3});
        jec_sf.push_back(ptL2L3 / ptRaw);
      }
      else {
        jec_sf.push_back(ptL3 / ptRaw);
      }
  }
  return jec_sf;
}

fRVec jetCorrections::get_jec_sf(fRVec A, fRVec eta, fRVec phi, fRVec pt, Float_t rho, fRVec rawF)
{
  fRVec jec_sf;
  for (size_t i = 0; i < pt.size(); i++) {
      double jetEta = eta[i];
      double jetPhi = phi[i];
      double ptRaw = pt[i] * (1 - rawF[i]);

      double ptL1 = ptRaw * jecL1.eval({A[i], jetEta, ptRaw, rho});
      double ptL2 = ptL1  * jecL2.eval({jetEta, jetPhi, ptL1});
      double ptL3 = ptL2  * jecL3.eval({jetEta, ptL2});

      if (datatype_ == "data") {
        double ptL2L3 = ptL3  * jecL2L3.eval({jetEta, ptL3});
        jec_sf.push_back(ptL2L3 / ptRaw);
      }
      else {
        jec_sf.push_back(ptL3 / ptRaw);
      }
  }
  return jec_sf;
}

fRVec jetCorrections::get_jec_sf(fRVec eta, fRVec pt, fRVec rawF, fRVec centralF, std::string drctn)
{
  fRVec jec_sf;
  for (size_t i = 0; i < pt.size(); i++) {
      double jetEta = eta[i];
      double ptRaw = pt[i] * (1 - rawF[i]);

      double shift = jecL1.eval({jetEta, ptRaw});
      double sf = centralF[i];
      if (drctn == "_up")        sf += shift;
      else if (drctn == "_down") sf -= shift;

      jec_sf.push_back(sf);
  }
  return jec_sf;
}

jerOutput jetCorrections::get_jer_outputs(
  int run, int luminosityBlock, int event,
  fRVec Jet_pt, fRVec Jet_eta, fRVec Jet_phi, fRVec Jet_mass,
  fRVec GenJet_pt, fRVec GenJet_eta, fRVec GenJet_phi, fRVec GenJet_mass,
  float rho)
{
  std::vector<uint64_t> jet_smear_seed;
  std::vector<float>  jet_smear_factor, jet_smear_factor_down,
                      jet_smear_factor_up, jet_pt_resolution_vector;

  // set seed
  int runnum = run << 20;
  int luminum = luminosityBlock << 10;
  int evtnum = event;
  int jet0eta = 0;
  if (Jet_eta.size() > 1) {
    jet0eta = int(Jet_eta[0] / 0.01);
  }
  uint64_t seed = 1 + runnum + evtnum + luminum + jet0eta;
  rnd.SetSeed(seed);

  // Loop over jets
  auto jet_tlv = TLorentzVector();
  float jet_pt_resolution = -1;
  for (size_t ijet = 0; ijet < Jet_pt.size(); ijet++) {
    jet_tlv.SetPtEtaPhiM(Jet_pt[ijet], Jet_eta[ijet], Jet_phi[ijet], Jet_mass[ijet]);
    
    jet_pt_resolution = jerPtRes.eval({jet_tlv.Eta(), jet_tlv.Pt(), rho});

    int genjet_index = -1;
    auto genjet_tlv = TLorentzVector();
    for (size_t igenjet = 0; igenjet < GenJet_pt.size(); igenjet++) {
      genjet_tlv.SetPtEtaPhiM(GenJet_pt[igenjet], GenJet_eta[igenjet], GenJet_phi[igenjet], GenJet_mass[igenjet]);
      
      if (jet_tlv.DeltaR(genjet_tlv) < 0.2 &&
          (fabs(jet_tlv.Pt() - genjet_tlv.Pt()) < 3 * jet_pt_resolution * jet_tlv.Pt())) {
        genjet_index = igenjet;
        break;
      }
    }

    std::vector<float> jet_pt_sf_and_uncertainty;
    for (size_t vars = 0; vars < variations.size(); vars++) {
      jet_pt_sf_and_uncertainty.push_back(
          jerSmearSf.eval({jet_tlv.Eta(), jet_tlv.Pt(), variations[vars]}));
    }

    std::vector<float> smear_vals;
    if (genjet_index != -1) {
      auto dPt = jet_tlv.Pt() - genjet_tlv.Pt();
      
      for (size_t vars = 0; vars < jet_pt_sf_and_uncertainty.size(); vars++) {
        smear_vals.push_back(1. + 
            (jet_pt_sf_and_uncertainty[vars] - 1.) * dPt / jet_tlv.Pt());
      }
    }

    else {
      auto rand = rnd.Gaus(0, jet_pt_resolution);
      for (size_t vars = 0; vars < jet_pt_sf_and_uncertainty.size(); vars++) {
        if (jet_pt_sf_and_uncertainty[vars] > 1.) {
          smear_vals.push_back(
            1. + rand * sqrt(std::pow(jet_pt_sf_and_uncertainty[vars], 2) - 1.));
        }
        else {
          smear_vals.push_back(1.);
        }
      }

      // check that smeared jet energy remains positive,
      // as the direction of the jet would change ("flip")
      // otherwise - and this is not what we want

      for (size_t vars = 0; vars < jet_pt_sf_and_uncertainty.size(); vars++) {
        if (smear_vals[vars] * jet_tlv.E() < 1E-2)
          smear_vals[vars] = 1E-2 / jet_tlv.E();
      }
    }

    jet_smear_seed.push_back(seed);
    jet_smear_factor.push_back(smear_vals[0]);
    jet_smear_factor_down.push_back(smear_vals[1]);
    jet_smear_factor_up.push_back(smear_vals[2]);
    jet_pt_resolution_vector.emplace_back(jet_pt_resolution);
  }

  jerOutput out({jet_smear_seed,
                 jet_smear_factor,
                 jet_smear_factor_down,
                 jet_smear_factor_up,
                 jet_pt_resolution_vector});

  return out;
}


jerOutput jetCorrections::get_jer_outputs(
  uint64_t event_seed,
  fRVec Jet_pt, fRVec Jet_eta, fRVec Jet_phi, fRVec Jet_mass,
  fRVec GenJet_pt, fRVec GenJet_eta, fRVec GenJet_phi, fRVec GenJet_mass,
  float rho)
{
  std::vector<uint64_t> jet_smear_seed;
  std::vector<float>  jet_smear_factor, jet_smear_factor_down,
                      jet_smear_factor_up, jet_pt_resolution_vector;

  // Loop over jets
  auto jet_tlv = TLorentzVector();
  float jet_pt_resolution = -1;
  for (size_t ijet = 0; ijet < Jet_pt.size(); ijet++) {
    uint64_t seed = RNGseed.object_seed(event_seed, ijet, 50);

    jet_tlv.SetPtEtaPhiM(Jet_pt[ijet], Jet_eta[ijet], Jet_phi[ijet], Jet_mass[ijet]);
    
    jet_pt_resolution = jerPtRes.eval({jet_tlv.Eta(), jet_tlv.Pt(), rho});

    int genjet_index = -1;
    auto genjet_tlv = TLorentzVector();
    for (size_t igenjet = 0; igenjet < GenJet_pt.size(); igenjet++) {
      genjet_tlv.SetPtEtaPhiM(GenJet_pt[igenjet], GenJet_eta[igenjet], GenJet_phi[igenjet], GenJet_mass[igenjet]);
      
      if (jet_tlv.DeltaR(genjet_tlv) < 0.2 &&
          (fabs(jet_tlv.Pt() - genjet_tlv.Pt()) < 3 * jet_pt_resolution * jet_tlv.Pt())) {
        genjet_index = igenjet;
        break;
      }
    }

    std::vector<float> jet_pt_sf_and_uncertainty;
    for (size_t vars = 0; vars < variations.size(); vars++) {
      jet_pt_sf_and_uncertainty.push_back(
          jerSmearSf.eval({jet_tlv.Eta(), jet_tlv.Pt(), variations[vars]}));
    }

    std::vector<float> smear_vals;
    if (genjet_index != -1) {
      auto dPt = jet_tlv.Pt() - genjet_tlv.Pt();
      
      for (size_t vars = 0; vars < jet_pt_sf_and_uncertainty.size(); vars++) {
        smear_vals.push_back(1. + 
            (jet_pt_sf_and_uncertainty[vars] - 1.) * dPt / jet_tlv.Pt());
      }
    }

    else {
      auto rand = RNGnumpy.normal(0, seed) * jet_pt_resolution;
      for (size_t vars = 0; vars < jet_pt_sf_and_uncertainty.size(); vars++) {
        if (jet_pt_sf_and_uncertainty[vars] > 1.) {
          smear_vals.push_back(
            1. + rand * sqrt(std::pow(jet_pt_sf_and_uncertainty[vars], 2) - 1.));
        }
        else {
          smear_vals.push_back(1.);
        }
      }

      // check that smeared jet energy remains positive,
      // as the direction of the jet would change ("flip")
      // otherwise - and this is not what we want

      for (size_t vars = 0; vars < jet_pt_sf_and_uncertainty.size(); vars++) {
        if (smear_vals[vars] * jet_tlv.E() < 1E-2)
          smear_vals[vars] = 1E-2 / jet_tlv.E();
      }
    }

    jet_smear_seed.push_back(seed);
    jet_smear_factor.push_back(smear_vals[0]);
    jet_smear_factor_down.push_back(smear_vals[1]);
    jet_smear_factor_up.push_back(smear_vals[2]);
    jet_pt_resolution_vector.emplace_back(jet_pt_resolution);
  }

  jerOutput out({jet_smear_seed,
                 jet_smear_factor,
                 jet_smear_factor_down,
                 jet_smear_factor_up,
                 jet_pt_resolution_vector});

  return out;
}
