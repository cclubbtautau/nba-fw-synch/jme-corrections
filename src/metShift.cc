#include "Corrections/JME/interface/metShift.h"

// Constructors
metShift::metShift () {}

// Destructor
metShift::~metShift() {}

std::vector<double> metShift::get_shifted_met(
    ROOT::VecOps::RVec<float> object_eta,
    ROOT::VecOps::RVec<float> object_phi,
    ROOT::VecOps::RVec<float> object_initial_pt,
    ROOT::VecOps::RVec<float> object_initial_mass,
    ROOT::VecOps::RVec<float> object_final_pt,
    ROOT::VecOps::RVec<float> object_final_mass,
    float Met_pt,
    float Met_phi,
    float object_pt_threshold
  )
{
  auto met_tlv = TLorentzVector();
  met_tlv.SetPxPyPzE(Met_pt * cos(Met_phi), Met_pt * sin(Met_phi), 0, Met_pt);
  for (size_t iJet = 0; iJet < object_initial_pt.size(); iJet++) {
    auto object_initial_tlv = TLorentzVector();
    object_initial_tlv.SetPtEtaPhiM(object_initial_pt[iJet], object_eta[iJet], object_phi[iJet], object_initial_mass[iJet]);
    auto object_final_tlv = TLorentzVector();
    object_final_tlv.SetPtEtaPhiM(object_final_pt[iJet], object_eta[iJet], object_phi[iJet], object_final_mass[iJet]);
    if (object_final_tlv.Pt() < object_pt_threshold) continue;
    met_tlv += object_initial_tlv;
    met_tlv -= object_final_tlv;
  }
  return {met_tlv.Pt(), met_tlv.Phi()};
};
