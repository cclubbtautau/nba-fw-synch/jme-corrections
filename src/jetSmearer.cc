#include "Corrections/JME/interface/jetSmearer.h"


// Constructors


jetSmearer::jetSmearer (
  std::string jerInputFilePath,
  std::string jerInputFileName,
  std::string jerUncertaintyInputFileName
)
{
  jerSF_and_Uncertainty = PyJetResolutionScaleFactorWrapper(jerInputFilePath
    + "/" + jerUncertaintyInputFileName);
  jer = PyJetResolutionWrapper(jerInputFilePath + "/" + jerInputFileName);
}

// Destructor
jetSmearer::~jetSmearer() {}

// std::vector<std::vector<float>> jetSmearer::get_smear_vals(
//std::vector<ROOT::VecOps::RVec<float>> jetSmearer::get_smear_vals(
jetSmearerOutput jetSmearer::get_smear_vals(
    int run,
    int luminosityBlock,
    int event,
    const ROOT::VecOps::RVec<float>& Jet_pt,
    const ROOT::VecOps::RVec<float>& Jet_eta,
    const ROOT::VecOps::RVec<float>& Jet_phi,
    const ROOT::VecOps::RVec<float>& Jet_mass,
    const ROOT::VecOps::RVec<float>& GenJet_pt,
    const ROOT::VecOps::RVec<float>& GenJet_eta,
    const ROOT::VecOps::RVec<float>& GenJet_phi,
    const ROOT::VecOps::RVec<float>& GenJet_mass,
    float rho
) {

  std::vector<uint64_t> jet_smear_seed;
  std::vector<float>  jet_smear_factor, jet_smear_factor_down, jet_smear_factor_up,
    jet_pt_resolution_vector;

  // set seed
  int runnum = run << 20;
  int luminum = luminosityBlock << 10;
  int evtnum = event;
  int jet0eta = 0;
  if (Jet_eta.size() > 1) {
    jet0eta = int(Jet_eta[0] / 0.01);
  }
  uint64_t seed = 1 + runnum + evtnum + luminum + jet0eta;
  rnd.SetSeed(seed);

  // Loop over jets
  auto jet_tlv = TLorentzVector();
  float jet_pt_resolution = -1;
  for (size_t ijet = 0; ijet < Jet_pt.size(); ijet++) {
    jet_tlv.SetPtEtaPhiM(Jet_pt[ijet], Jet_eta[ijet], Jet_phi[ijet], Jet_mass[ijet]);\
    params_resolution.setJetPt(jet_tlv.Perp());
    params_resolution.setJetEta(jet_tlv.Eta());
    params_resolution.setRho(rho);
    jet_pt_resolution = jer.getResolution(params_resolution);

    int genjet_index = -1;
    auto genjet_tlv = TLorentzVector();
    for (size_t igenjet = 0; igenjet < GenJet_pt.size(); igenjet++) {
      genjet_tlv.SetPtEtaPhiM(
        GenJet_pt[igenjet], GenJet_eta[igenjet], GenJet_phi[igenjet], GenJet_mass[igenjet]);
      if (jet_tlv.DeltaR(genjet_tlv) < 0.2 && (
          fabs(jet_tlv.Pt() - genjet_tlv.Pt()) < 3 * jet_pt_resolution * jet_tlv.Pt())) {
        genjet_index = igenjet;
        break;
      }
    }

    params_sf_and_uncertainty.setJetEta(jet_tlv.Eta());
    params_sf_and_uncertainty.setJetPt(jet_tlv.Pt());

    std::vector <Variation> variations = {Variation::NOMINAL, Variation::DOWN, Variation::UP};

    std::vector<float> jet_pt_sf_and_uncertainty;
    for (size_t central_or_shift = 0; central_or_shift < variations.size(); central_or_shift++) {
      jet_pt_sf_and_uncertainty.push_back(
        jerSF_and_Uncertainty.getScaleFactor(
          params_sf_and_uncertainty, variations[central_or_shift]));
    }

    std::vector<float> smear_vals;
    if (genjet_index != -1) {
      auto dPt = jet_tlv.Perp() - genjet_tlv.Perp();
      for (size_t central_or_shift = 0; central_or_shift < jet_pt_sf_and_uncertainty.size();
          central_or_shift++) {
        smear_vals.push_back(1. + (
          jet_pt_sf_and_uncertainty[central_or_shift] - 1.) * dPt / jet_tlv.Perp());
      }
    } else {
      auto rand = rnd.Gaus(0, jet_pt_resolution);
      for (size_t central_or_shift = 0; central_or_shift < jet_pt_sf_and_uncertainty.size();
          central_or_shift++) {
        if (jet_pt_sf_and_uncertainty[central_or_shift] > 1.) {
          smear_vals.push_back(
            1. + rand * sqrt(std::pow(jet_pt_sf_and_uncertainty[central_or_shift], 2) - 1.));
        } else {
          smear_vals.push_back(1.);
        }
      }

      // check that smeared jet energy remains positive,
      // as the direction of the jet would change ("flip")
      // otherwise - and this is not what we want

      for (size_t central_or_shift = 0; central_or_shift < jet_pt_sf_and_uncertainty.size();
          central_or_shift++) {
        if (smear_vals[central_or_shift] * jet_tlv.E() < 1E-2)
          smear_vals[central_or_shift] = 1E-2 / jet_tlv.E();
      }
    }
    jet_smear_seed.push_back(seed);
    jet_smear_factor.push_back(smear_vals[0]);
    jet_smear_factor_down.push_back(smear_vals[1]);
    jet_smear_factor_up.push_back(smear_vals[2]);
    jet_pt_resolution_vector.emplace_back(jet_pt_resolution);
  }

  jetSmearerOutput out({jet_smear_seed,
                        jet_smear_factor,
                        jet_smear_factor_down,
                        jet_smear_factor_up,
                        jet_pt_resolution_vector});

  return out;
}

jetSmearerOutput jetSmearer::get_smear_vals(
    uint64_t event_seed,
    const ROOT::VecOps::RVec<float>& Jet_pt,
    const ROOT::VecOps::RVec<float>& Jet_eta,
    const ROOT::VecOps::RVec<float>& Jet_phi,
    const ROOT::VecOps::RVec<float>& Jet_mass,
    const ROOT::VecOps::RVec<float>& GenJet_pt,
    const ROOT::VecOps::RVec<float>& GenJet_eta,
    const ROOT::VecOps::RVec<float>& GenJet_phi,
    const ROOT::VecOps::RVec<float>& GenJet_mass,
    float rho
) {

  std::vector<uint64_t> jet_smear_seed;
  std::vector<float>  jet_smear_factor, jet_smear_factor_down, jet_smear_factor_up,
    jet_pt_resolution_vector;

  // Loop over jets
  auto jet_tlv = TLorentzVector();
  float jet_pt_resolution = -1;
  for (size_t ijet = 0; ijet < Jet_pt.size(); ijet++) {
    uint64_t seed = RNGseed.object_seed(event_seed, ijet, 50);

    jet_tlv.SetPtEtaPhiM(Jet_pt[ijet], Jet_eta[ijet], Jet_phi[ijet], Jet_mass[ijet]);\
    params_resolution.setJetPt(jet_tlv.Perp());
    params_resolution.setJetEta(jet_tlv.Eta());
    params_resolution.setRho(rho);
    jet_pt_resolution = jer.getResolution(params_resolution);

    int genjet_index = -1;
    auto genjet_tlv = TLorentzVector();
    for (size_t igenjet = 0; igenjet < GenJet_pt.size(); igenjet++) {
      genjet_tlv.SetPtEtaPhiM(
        GenJet_pt[igenjet], GenJet_eta[igenjet], GenJet_phi[igenjet], GenJet_mass[igenjet]);
      if (jet_tlv.DeltaR(genjet_tlv) < 0.2 && (
          fabs(jet_tlv.Pt() - genjet_tlv.Pt()) < 3 * jet_pt_resolution * jet_tlv.Pt())) {
        genjet_index = igenjet;
        break;
      }
    }

    params_sf_and_uncertainty.setJetEta(jet_tlv.Eta());
    params_sf_and_uncertainty.setJetPt(jet_tlv.Pt());

    std::vector <Variation> variations = {Variation::NOMINAL, Variation::DOWN, Variation::UP};

    std::vector<float> jet_pt_sf_and_uncertainty;
    for (size_t central_or_shift = 0; central_or_shift < variations.size(); central_or_shift++) {
      jet_pt_sf_and_uncertainty.push_back(
        jerSF_and_Uncertainty.getScaleFactor(
          params_sf_and_uncertainty, variations[central_or_shift]));
    }

    std::vector<float> smear_vals;
    if (genjet_index != -1) {
      auto dPt = jet_tlv.Perp() - genjet_tlv.Perp();
      for (size_t central_or_shift = 0; central_or_shift < jet_pt_sf_and_uncertainty.size();
          central_or_shift++) {
        smear_vals.push_back(1. + (
          jet_pt_sf_and_uncertainty[central_or_shift] - 1.) * dPt / jet_tlv.Perp());
      }
    } else {
      auto rand = RNGnumpy.normal(0, seed) * jet_pt_resolution;
      for (size_t central_or_shift = 0; central_or_shift < jet_pt_sf_and_uncertainty.size();
          central_or_shift++) {
        if (jet_pt_sf_and_uncertainty[central_or_shift] > 1.) {
          smear_vals.push_back(
            1. + rand * sqrt(std::pow(jet_pt_sf_and_uncertainty[central_or_shift], 2) - 1.));
        } else {
          smear_vals.push_back(1.);
        }
      }

      // check that smeared jet energy remains positive,
      // as the direction of the jet would change ("flip")
      // otherwise - and this is not what we want

      for (size_t central_or_shift = 0; central_or_shift < jet_pt_sf_and_uncertainty.size();
          central_or_shift++) {
        if (smear_vals[central_or_shift] * jet_tlv.E() < 1E-2)
          smear_vals[central_or_shift] = 1E-2 / jet_tlv.E();
      }
    }
    jet_smear_seed.push_back(seed);
    jet_smear_factor.push_back(smear_vals[0]);
    jet_smear_factor_down.push_back(smear_vals[1]);
    jet_smear_factor_up.push_back(smear_vals[2]);
    jet_pt_resolution_vector.emplace_back(jet_pt_resolution);
  }

  jetSmearerOutput out({jet_smear_seed,
                        jet_smear_factor,
                        jet_smear_factor_down,
                        jet_smear_factor_up,
                        jet_pt_resolution_vector});

  return out;
}
